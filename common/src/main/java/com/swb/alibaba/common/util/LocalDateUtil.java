package com.swb.alibaba.common.util;

import cn.hutool.core.date.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.annotation.DateTimeFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

/**
 * <p>文件  LocalDateUtil</p>
 * <p>时间  2021-06-30 23:06:06</p>
 *
 * @author swb
 */
public class LocalDateUtil {
    private static final Logger log = LoggerFactory.getLogger(LocalDateUtil.class);
    public static final DateTimeFormatter FORMAT_DAY = DateTimeFormatter.ofPattern("yyyy-MM-dd 00:00:00");
    public static final DateTimeFormatter FORMAT_MONTH = DateTimeFormatter.ofPattern("yyyy-MM-01 00:00:00");
    public static final DateTimeFormatter FORMAT_YEAR = DateTimeFormatter.ofPattern("yyyy-01-01 00:00:00");
    public static final DateTimeFormatter FORMAT_TIME = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    private LocalDateUtil() {
    }

    public static String getCurrentTime() {
        return FORMAT_TIME.format(LocalDateTime.now());
    }

    public static String getDate(String formatStr, int num) {
        String date;
        switch(num) {
            case 1:
                date = formatDate(getCurrYearFirst(), formatStr);
                break;
            case 12:
                date = formatDate(getCurrYearLast(), formatStr);
                break;
            default:
                date = formatDate(new Date(), formatStr);
        }

        return date;
    }

    /** @deprecated */
    @Deprecated
    public static String formatDate(Date date, String param) {
        byte var4 = -1;
        switch(param.hashCode()) {
            case 77:
                if (param.equals("M")) {
                    var4 = 0;
                }
                break;
            case 89:
                if (param.equals("Y")) {
                    var4 = 1;
                }
        }

        String formatStr;
        switch(var4) {
            case 0:
                formatStr = "yyyy-MM";
                break;
            case 1:
                formatStr = "yyyy";
                break;
            default:
                formatStr = "yyyy-MM-dd";
        }

        SimpleDateFormat f = new SimpleDateFormat(formatStr);
        return f.format(date);
    }

    public static Date getCurrYearFirst() {
        Calendar currCal = Calendar.getInstance();
        int currentYear = currCal.get(1);
        return getYearFirst(currentYear);
    }

    public static Date getCurrYearLast() {
        Calendar currCal = Calendar.getInstance();
        int currentYear = currCal.get(1);
        return getYearLast(currentYear);
    }

    public static Date getYearFirst(int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(1, year);
        return calendar.getTime();
    }

    public static Date getYearLast(int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(1, year);
        calendar.roll(6, -1);
        return calendar.getTime();
    }

    public static Date datePlusDays(int days) {
        Calendar cal = Calendar.getInstance();
        cal.add(5, days);
        return cal.getTime();
    }

    public static Date datePlusDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(5, days);
        return cal.getTime();
    }

    public static Date localDate2Date(LocalDate localDate) {
        if (null == localDate) {
            return null;
        } else {
            ZonedDateTime zonedDateTime = localDate.atStartOfDay(ZoneId.systemDefault());
            return Date.from(zonedDateTime.toInstant());
        }
    }

    public static Date localDateTime2Date(LocalDateTime localDateTime) {
        if (null == localDateTime) {
            return null;
        } else {
            ZonedDateTime zonedDateTime = localDateTime.atZone(ZoneId.systemDefault());
            return Date.from(zonedDateTime.toInstant());
        }
    }

    public static LocalDate date2LocalDate(Date date) {
        return null == date ? null : Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static LocalDateTime date2LocalDateTime(Date date) {
        return null == date ? null : Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    public static Date str2date(String dateStr, String formatStr) {
        SimpleDateFormat sdf = new SimpleDateFormat(formatStr);
        Date date = null;

        try {
            date = sdf.parse(dateStr);
        } catch (ParseException var5) {
            log.error("字符串转日期错误: 日期字符串={}，格式={}", dateStr, formatStr);
        }

        return date;
    }
}
