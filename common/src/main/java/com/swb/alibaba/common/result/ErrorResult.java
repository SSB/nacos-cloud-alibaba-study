package com.swb.alibaba.common.result;

/**
 * <p>文件  ErrorResult</p>
 * <p>时间  2021-06-30 22:34:30</p>
 *
 * @author swb
 */
public class ErrorResult extends Result {
    private Error error;

    public ErrorResult(Error error) {
        this.setIsError(true);
        this.error = error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public Error getError() {
        return this.error;
    }
}
