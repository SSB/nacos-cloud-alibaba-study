package com.swb.alibaba.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>文件  Util</p>
 * <p>时间  2021-06-30 22:30:21</p>
 *
 * @author swb
 */
public class Util {

    public Util() {
    }

    public static String nvl(String s) {
        return s == null ? "" : replaceSpecialSpace(s).trim();
    }

    public static String nvlWithTrim(String s) {
        return s == null ? "" : replaceAllSpace(s);
    }

    public static String[] split(String str, String splitment) {
        String[] arr = null;
        if (str != null && str.length() > 0) {
            arr = str.split(splitment);
        }
        return arr;
    }

    public static String nvl(Object o) {
        return o == null ? "" : replaceSpecialSpace(o.toString()).trim();
    }

    public static String nvlWithTrim(Object o) {
        return o == null ? "" : replaceAllSpace(o.toString());
    }

    private static String replaceAllSpace(String str) {
        return str.replaceAll("\\s|\\u00A0|\\u0020|\\u3000*", "");
    }

    private static String replaceSpecialSpace(String str) {
        return str.replaceAll("\\u00A0|\\u3000*", "");
    }

    public static String nvl(Object o, String replacement) {
        String s = nvl(o);
        return s.length() == 0 ? replacement : s;
    }

    public static boolean isNumeric(String str) {
        if (str == null) {
            return false;
        } else {
            try {
                Double.parseDouble(str);
                return true;
            } catch (Exception var2) {
                return false;
            }
        }
    }

    public static boolean isNumeric(Object str) {
        if (str == null) {
            return false;
        } else {
            try {
                Double.parseDouble((String) str);
                return true;
            } catch (Exception var2) {
                return false;
            }
        }
    }

    public static int str2num(Object s) {
        if (s == null) {
            return 0;
        } else {
            return "".equals(s) ? 0 : Integer.parseInt(nvl(s));
        }
    }

    public static Date strToDate(String str) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        if (str != null && str.length() > 0) {
            try {
                date = format.parse(str);
            } catch (ParseException var4) {
                var4.printStackTrace();
            }
        }
        return date;
    }

    public static String removeLastChar(Object o) {
        String s = nvl(o);
        if (s.length() > 0) {
            s = s.substring(0, s.length() - 1);
        }
        return s;
    }

    public static String firstToLowerCase(String str) {
        char[] chars = str.toCharArray();
        chars[0] = Character.toLowerCase(chars[0]);
        return new String(chars);
    }

    public static String toHumpAndFirstToLower(String str) {
        if (str.contains("_")) {
            str = str.toLowerCase();
            char[] chars = str.toCharArray();
            for (int i = 0; i < chars.length; ++i) {
                if (chars[i] == '_') {
                    ++i;
                    if (i < chars.length) {
                        chars[i] = Character.toUpperCase(chars[i]);
                    }
                }
            }
            return (new String(chars)).replaceAll("_", "");
        } else {
            return str;
        }
    }

    public static String headToUpper(String str) {
        char[] cs = str.toCharArray();
        cs[0] = (char) (cs[0] - 32);
        return String.valueOf(cs);
    }

    public static String decodeGeohash(String geohash) {
        int numbits = 30;
        char[] digits = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'm', 'n', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
        HashMap<Character, Integer> lookup = new HashMap();
        int ii = 0;
        char[] var5 = digits;
        int var6 = digits.length;

        int var7;
        for (var7 = 0; var7 < var6; ++var7) {
            char c = var5[var7];
            lookup.put(c, ii++);
        }

        StringBuilder buffer = new StringBuilder();
        char[] var14 = geohash.toCharArray();
        var7 = var14.length;

        int j;
        for (j = 0; j < var7; ++j) {
            char c = var14[j];
            int i = (Integer) lookup.get(c) + 32;
            buffer.append(Integer.toString(i, 2).substring(1));
        }

        BitSet lngset = new BitSet();
        BitSet latset = new BitSet();
        j = 0;

        int i;
        boolean isSet;
        for (i = 0; i < numbits * 2; i += 2) {
            isSet = false;
            if (i < buffer.length()) {
                isSet = buffer.charAt(i) == '1';
            }
            lngset.set(j++, isSet);
        }

        j = 0;

        for (i = 1; i < numbits * 2; i += 2) {
            isSet = false;
            if (i < buffer.length()) {
                isSet = buffer.charAt(i) == '1';
            }
            latset.set(j++, isSet);
        }

        double lng = decode(lngset, -180.0D, 180.0D);
        double lat = decode(latset, -90.0D, 90.0D);
        return lng + "," + lat;
    }

    private static double decode(BitSet bs, double floor, double ceiling) {
        double mid = 0.0D;
        for (int i = 0; i < bs.length(); ++i) {
            mid = (floor + ceiling) / 2.0D;
            if (bs.get(i)) {
                floor = mid;
            } else {
                ceiling = mid;
            }
        }

        return mid;
    }

    public static Object[] removeArrayEmptyText(Object[] array) {
        List<Object> listNew = new ArrayList<>();
        Object[] var2 = array;
        int var3 = array.length;

        for (int var4 = 0; var4 < var3; ++var4) {
            Object o = var2[var4];
            if (o != null && !"".equals(o)) {
                listNew.add(o);
            }
        }

        return listNew.toArray();
    }

    public static String lpad(String s, int n, String replace) {
        StringBuilder buf = new StringBuilder();
        int remain = n - s.length();

        for (int i = 0; i < remain; ++i) {
            buf.append(replace);
        }

        return buf.append(s).toString();
    }

    public static byte[] intToByteArray(int n) {
        byte[] b = new byte[]{(byte) (n & 255), (byte) (n >> 8 & 255), (byte) (n >> 16 & 255), (byte) (n >> 24 & 255)};
        return b;
    }

    public static int byteArrayToInt(byte[] bArr) {
        return bArr.length != 4 ? -1 : (bArr[3] & 255) << 24 | (bArr[2] & 255) << 16 | (bArr[1] & 255) << 8 | bArr[0] & 255;
    }

    public static boolean isNullOrEmpty(String value) {
        return value == null || value.length() == 0;
    }

    public static boolean isNullOrEmpty(Object value) {
        return value == null || value.toString().length() == 0;
    }

    public static boolean isNullOrEmpty(List value) {
        return value == null || value.size() == 0;
    }

    public static boolean isNull(Object value) {
        return value == null;
    }

    public static boolean isBlank(String str) {
        int strLen;
        if (str != null && (strLen = str.length()) != 0) {
            for (int i = 0; i < strLen; ++i) {
                if (!Character.isWhitespace(str.charAt(i))) {
                    return false;
                }
            }
            return true;
        } else {
            return true;
        }
    }

    public static boolean containsSqlInjection(String sql) {
        Pattern pattern = Pattern.compile("\\b(and|exec|insert|select|drop|grant|alter|delete|update|count|chr|mid|master|truncate|char|declare|or)\\b|([*;+'%])");
        Matcher matcher = pattern.matcher(sql.toLowerCase());
        return matcher.find();
    }
}
