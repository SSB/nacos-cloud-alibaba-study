package com.swb.alibaba.common.util;

import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <p>文件  JsonUtil</p>
 * <p>时间  2021-06-30 22:43:32</p>
 *
 * @author swb
 */
public class JsonUtil {
    private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final ObjectMapper MAPPER;
    private static final ObjectMapper MAPPER_NON_NULL;
    private static final List<String> EXCLUDE_CLASS_NAMES = Arrays.asList("RequestFacade", "ResponseFacade");
    private static final String EXCLUDE_ARRAY_CLASS_NAME = "Object[]";

    static {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        MAPPER = new ObjectMapper();
        MAPPER.setVisibility(PropertyAccessor.SETTER, Visibility.NONE);
        MAPPER.setVisibility(PropertyAccessor.GETTER, Visibility.NONE);
        MAPPER.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
        MAPPER.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        MAPPER.setDateFormat(dateFormat);
        MAPPER_NON_NULL = new ObjectMapper();
        MAPPER_NON_NULL.setVisibility(PropertyAccessor.SETTER, Visibility.NONE);
        MAPPER_NON_NULL.setVisibility(PropertyAccessor.GETTER, Visibility.NONE);
        MAPPER_NON_NULL.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
        MAPPER_NON_NULL.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        MAPPER_NON_NULL.setSerializationInclusion(Include.NON_NULL);
        MAPPER_NON_NULL.setDateFormat(dateFormat);
    }



    public JsonUtil() {
    }

    public static String toJson(Object obj) {
        String json = "";
        if (obj != null) {
            try {
                json = MAPPER.writeValueAsString(objectSerializeFilter(obj));
            } catch (Exception var3) {
                json = "[转换json字符失败]";
            }
        }

        return json;
    }

    public static String toJsonPretty(Object obj) {
        String json = "";
        if (obj != null) {
            try {
                json = MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(objectSerializeFilter(obj));
            } catch (Exception var3) {
                json = "[转换json字符失败]";
            }
        }

        return json;
    }

    public static String toJsonNonNull(Object obj) {
        String json = "";
        if (obj != null) {
            try {
                json = MAPPER_NON_NULL.writeValueAsString(objectSerializeFilter(obj));
            } catch (Exception var3) {
                json = "[转换json字符失败]";
            }
        }

        return json;
    }

    public static String toJsonNonNullPretty(Object obj) {
        String json = "";
        if (obj != null) {
            try {
                json = MAPPER_NON_NULL.writerWithDefaultPrettyPrinter().writeValueAsString(objectSerializeFilter(obj));
            } catch (Exception var3) {
                json = "[转换json字符失败]";
            }
        }

        return json;
    }

    public static String toJson(Object[] objectArray) {
        String json = "";
        if (objectArray != null) {
            try {
                if ("Object[]".equals(objectArray.getClass().getSimpleName())) {
                    json = MAPPER.writeValueAsString(arraySerializeFilter(objectArray));
                } else {
                    json = MAPPER.writeValueAsString(objectArray);
                }
            } catch (Exception var3) {
                json = "[转换json字符失败]";
            }
        }

        return json;
    }

    public static <T> T toObject(String json, Class<T> clazz) {
        try {
            return MAPPER.readValue(json, clazz);
        } catch (IOException var3) {
            LoggerUtil.error("将json字符转换为对象时失败!", var3);
            throw new RuntimeException("将json字符转换为对象时失败!");
        }
    }

    public static <T> T toObject(URL url, Class<T> clazz) {
        try {
            return MAPPER.readValue(url, clazz);
        } catch (IOException var3) {
            LoggerUtil.error("将json字符转换为对象时失败!", var3);
            throw new RuntimeException("将json字符转换为对象时失败!");
        }
    }

    public static <T> T toObject(String json, TypeReference tr) {
        try {
            return (T) MAPPER.readValue(json, tr);
        } catch (IOException var3) {
            LoggerUtil.error("将json字符转换为对象时失败", var3);
            throw new RuntimeException("将json字符转换为对象时失败!");
        }
    }

    private static List<Object> arraySerializeFilter(Object[] args) {
        List<Object> allowObjects = new ArrayList();
        Object[] var2 = args;
        int var3 = args.length;

        for(int var4 = 0; var4 < var3; ++var4) {
            Object o = var2[var4];
            if (!EXCLUDE_CLASS_NAMES.contains(o.getClass().getSimpleName())) {
                allowObjects.add(o);
            }
        }

        return allowObjects;
    }

    private static Object objectSerializeFilter(Object object) {
        return !EXCLUDE_CLASS_NAMES.contains(object.getClass().getSimpleName()) ? object : null;
    }


}
