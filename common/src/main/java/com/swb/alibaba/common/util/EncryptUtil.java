package com.swb.alibaba.common.util;

import org.apache.tomcat.util.codec.binary.Base64;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

/**
 * <p>文件  EncryptUtil</p>
 * <p>时间  2021-06-30 22:59:36</p>
 *
 * @author swb
 */
public class EncryptUtil {
    private static final String KEY_SHA = "SHA";
    private static final String KEY_MD5 = "MD5";
    private static final int SALT_SIZE = 6;

    public EncryptUtil() {
    }

    public static String decryptBASE64(String key) throws Exception {
        return new String(Base64.decodeBase64(key));
    }

    public static String encryptBASE64(String key) {
        return Base64.encodeBase64String(key.getBytes());
    }

    public static String encryptMD5(String data) {
        return byteArrayToHex(getMD5Digest(data));
    }

    public static String encryptMD5(String data, String salt) {
        return encryptMD5(data + salt);
    }

    private static byte[] getMD5Digest(String data) {
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.update(data.getBytes("UTF-8"));
            return md5.digest();
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException var2) {
            var2.printStackTrace();
            return new byte[0];
        }
    }

    public static String encryptRandomMD5(String data) {
        Random random = new Random();
        byte[] salts = new byte[6];
        StringBuilder dataBuilder = new StringBuilder(data);

        for(int i = 0; i < 6; ++i) {
            int salt = random.nextInt(256);
            dataBuilder.append(salt);
            salts[i] = (byte)salt;
        }

        data = dataBuilder.toString();
        return byteArrayToHex(insertItems(getMD5Digest(data), salts));
    }

    public static boolean equalsRandomMD5(String data, String cipherText) {
        byte[] bytes = hexToByteArray(cipherText);
        String newCipherText = byteArrayToHex(insertItems(getMD5Digest(data + getStringSalt(bytes)), getByteArraySalt(bytes)));
        return cipherText.equals(newCipherText);
    }

    private static byte[] insertItems(byte[] bytes, byte[] items) {
        int len = items.length;
        byte[] newBytes = new byte[bytes.length + len];
        System.arraycopy(items, 0, newBytes, 0, len);
        System.arraycopy(bytes, 0, newBytes, len, bytes.length);
        return newBytes;
    }

    private static String byteArrayToHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        byte[] var2 = bytes;
        int var3 = bytes.length;

        for(int var4 = 0; var4 < var3; ++var4) {
            byte b = var2[var4];
            sb.append(String.format("%02X", b));
        }

        return sb.toString();
    }

    private static byte[] hexToByteArray(String hex) {
        hex = hex.length() % 2 != 0 ? "0" + hex : hex;
        byte[] b = new byte[hex.length() / 2];

        for(int i = 0; i < b.length; ++i) {
            int index = i * 2;
            int v = Integer.parseInt(hex.substring(index, index + 2), 16);
            b[i] = (byte)v;
        }

        return b;
    }

    private static byte[] getByteArraySalt(byte[] bytes) {
        byte[] salts = new byte[6];
        System.arraycopy(bytes, 0, salts, 0, 6);
        return salts;
    }

    private static String getStringSalt(byte[] bytes) {
        StringBuilder saltStr = new StringBuilder();

        for(int i = 0; i < 6; ++i) {
            byte salt = bytes[i];
            saltStr.append(salt < 0 ? 256 + salt : salt);
        }

        return saltStr.toString();
    }

    public static String encryptSHA(String data) {
        StringBuilder sb = new StringBuilder();

        try {
            MessageDigest sha = MessageDigest.getInstance("SHA");
            sha.update(data.getBytes());
            byte[] var3 = sha.digest();
            int var4 = var3.length;

            for(int var5 = 0; var5 < var4; ++var5) {
                byte b = var3[var5];
                sb.append(String.format("%02X", b));
            }

            return sb.toString();
        } catch (NoSuchAlgorithmException var7) {
            var7.printStackTrace();
            return data;
        }
    }
}
