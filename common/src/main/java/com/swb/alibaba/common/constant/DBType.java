package com.swb.alibaba.common.constant;

import java.util.Arrays;
import java.util.List;

/**
 * <p>文件  DBType</p>
 * <p>时间  2021-06-30 22:53:39</p>
 *
 * @author swb
 */
public class DBType {
    public static final String MYSQL = "mysql";
    public static final String GREENPLUM = "greenplum";
    public static final String ORACLE = "oracle";
    public static final String ODPS = "odps";
    public static final String POSTGRESQL = "postgresql";
    public static final String SQLSERVER = "sqlserver";
    public static final String DRDS = "drds";
    public static final String FTP = "ftp";
    public static final String HABSE11X = "hbase11x";
    public static final String HBASE094X = "hbase094x";
    public static final String HDFS = "hdfs";
    public static final String MONGODB = "mongodb";
    public static final String OSS = "oss";
    public static final String OTS = "ots";
    public static final String OTSSTREAM = "otsstream";
    public static final String RDBMS = "rdbms";
    public static final String STREAM = "stream";
    public static final String TXTFILE = "txtfile";
    public static final String ADS = "ads";
    public static final String HABSE11XSQL = "hbase11xsql";
    public static final String OCS = "ocs";
    public static final List<String> JDBC_TYPE = Arrays.asList("mysql", "greenplum", "postgresql", "sqlserver");

    public DBType() {
    }
}
