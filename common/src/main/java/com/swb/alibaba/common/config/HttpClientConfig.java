package com.swb.alibaba.common.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * <p>文件  HttpClientConfig</p>
 * <p>时间  2021-06-30 23:18:15</p>
 *
 * @author swb
 */
@Component
public class HttpClientConfig {
    @Value("${httpClient.connectTimeout:10000}")
    private int connectTimeout;
    @Value("${httpClient.readTimeout:5000}")
    private int readTimeout;

    public HttpClientConfig() {
    }

    public void setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public void setReadTimeout(int readTimeout) {
        this.readTimeout = readTimeout;
    }

    public int getConnectTimeout() {
        return this.connectTimeout;
    }

    public int getReadTimeout() {
        return this.readTimeout;
    }
}
