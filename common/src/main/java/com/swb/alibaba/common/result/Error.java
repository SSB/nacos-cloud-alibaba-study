package com.swb.alibaba.common.result;

import com.swb.alibaba.common.enums.IErrorEnum;

/**
 * <p>文件  Error</p>
 * <p>时间  2021-06-30 22:34:52</p>
 *
 * @author swb
 */
public class Error {
    private Integer codeNumber;
    private String message;
    private String code;

    public Error() {
    }

    public Error(IErrorEnum re) {
        this.codeNumber = re.getCodeNumber();
        this.message = re.getMessage();
        this.code = re.getCode();
    }

    public Error(Integer codeNumber, String message, String code) {
        this.codeNumber = codeNumber;
        this.message = message;
        this.code = code;
    }

    public Error(Integer codeNumber, String message) {
        this.codeNumber = codeNumber;
        this.message = message;
        this.code = "UncaughtException";
    }

    public Integer getCodeNumber() {
        return this.codeNumber;
    }

    public void setCodeNumber(Integer codeNumber) {
        this.codeNumber = codeNumber;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
