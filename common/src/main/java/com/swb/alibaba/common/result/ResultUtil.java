package com.swb.alibaba.common.result;

import com.swb.alibaba.common.enums.ErrorEnum;
import com.swb.alibaba.common.enums.IErrorEnum;
import com.swb.alibaba.common.util.Util;

/**
 * <p>文件  ResultUtil</p>
 * <p>时间  2021-06-30 22:36:16</p>
 *
 * @author swb
 */
public class ResultUtil {

    public static Result success(Object object) {
        return getSuccessResult(object);
    }

    public static Result success() {
        return getSuccessResult((Object) null);
    }

    public static Result error() {
        return getErrorResult((IErrorEnum) ErrorEnum.ERROR);
    }

    public static Result error(IErrorEnum re) {
        return getErrorResult(re);
    }

    /**
     * @deprecated
     */
    @Deprecated
    public static Result error(Integer code, String msg) {
        return new ErrorResult(new Error(code, msg));
    }

    public static Result error(Error error) {
        return getErrorResult(error);
    }

    public static Result error(String msg) {
        return getErrorResult(msg);
    }

    private static Result getSuccessResult(Object object) {
        return new SuccessResult(object);
    }

    private static Result getErrorResult(Error e) {
        return new ErrorResult(e);
    }

    private static Result getErrorResult(IErrorEnum re) {
        return new ErrorResult(new Error(re));
    }

    private static Result getErrorResult(String msg) {
        Error errorInfo = new Error(ErrorEnum.ERROR);
        errorInfo.setMessage(msg);
        return getErrorResult(errorInfo);
    }
}
