package com.swb.alibaba.common.result;

/**
 * <p>文件  Result</p>
 * <p>时间  2021-06-30 22:28:45</p>
 *
 * @author swb
 */
public abstract class Result {
    private Boolean isError = false;

    public Result() {
    }

    public void setIsError(Boolean isError) {
        this.isError = isError;
    }

    public Boolean getIsError() {
        return this.isError;
    }
}
