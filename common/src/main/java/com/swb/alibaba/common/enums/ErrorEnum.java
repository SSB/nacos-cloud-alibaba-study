package com.swb.alibaba.common.enums;

import com.swb.alibaba.common.util.Util;

/**
 * <p>文件  ErrorEnum</p>
 * <p>时间  2021-06-30 22:38:07</p>
 *
 * @author swb
 */
public enum ErrorEnum implements IErrorEnum {
    LOGIN_ERROR_LOGGED(900, "抱歉，您的账号已于其他终端登陆，您将被强制下线", "AccountIsLoggedInException"),
    LOGIN_ERROR(901, "用户名或密码错误", "IncorrectException"),
    LOGIN_ERROR_LOCKED(902, "账户已锁定", "LockedAccountException"),
    LOGIN_ERROR_EXCESS(903, "用户名或密码错误次数过多", "ExcessiveAttemptsException"),
    LOGIN_ERROR_RELOGIN(904, "登录状态已失效，请重新登录", "UnauthenticatedException"),
    ERROR(1001, "操作失败", "FailedException"),
    UNAUTHORIZED_ERROR(1002, "无操作权限", "UnauthorizedException"),
    EXCEPTION_ERROR(1003, "服务端程序异常错误", "UncaughtException"),
    ILLEGAL_ARGUMENT_ERROR(1004, "参数不合法或不正确", "IllegalArgumentException"),
    REMOTE_SERVER_ERROR(1005, "远程验证服务器出错", "RemoteServerException");

    private final Integer codeNumber;
    private final String message;
    private final String code;

    private ErrorEnum(Integer codeNumber, String message, String code) {
        this.codeNumber = codeNumber;
        this.message = message;
        this.code = code;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public Integer getCodeNumber() {
        return this.codeNumber;
    }

    @Override
    public String getCodeNumString() {
        return Util.nvl(this.codeNumber);
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
