package com.swb.alibaba.common.handler;

import com.swb.alibaba.common.excetions.DynamicMsgException;
import com.swb.alibaba.common.result.Result;
import com.swb.alibaba.common.result.Error;
import com.swb.alibaba.common.result.ResultUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Arrays;
import java.util.List;

@RestControllerAdvice
public class RestExceptionHandler {
    private static final Logger log = LoggerFactory.getLogger(RestExceptionHandler.class);
    private static final List<String> IGNORE_EXCEPTIONS = Arrays.asList("HttpRequestMethodNotSupportedException");

    public RestExceptionHandler() {
    }

    @ExceptionHandler({RuntimeException.class})
    public Result handleRuntimeException(RuntimeException e) {
        return ResultUtil.error("出现未知错误,请联系客服");
    }

    @ExceptionHandler({DynamicMsgException.class})
    public Result handleDynamicMsgException(DynamicMsgException e) {
        log.error(e.toString(), e);
        Error error=new Error(1010,e.getMessage(),"DynamicException");
        return ResultUtil.error(error);
    }
}