package com.swb.alibaba.common.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * <p>文件  NetUtil</p>
 * <p>时间  2021-06-30 23:01:04</p>
 *
 * @author swb
 */
public class NetUtil {
    public NetUtil() {
    }

    public static String download(String urlStr) {
        StringBuffer sb = new StringBuffer();
        String line = null;
        BufferedReader buffer = null;

        try {
            URL url = new URL(urlStr);
            HttpURLConnection urlConn = (HttpURLConnection)url.openConnection();
            buffer = new BufferedReader(new InputStreamReader(urlConn.getInputStream(), "utf-8"));

            while((line = buffer.readLine()) != null) {
                sb.append(line);
            }
        } catch (Exception var14) {
            sb.append("ERROR");
            var14.printStackTrace();
        } finally {
            try {
                buffer.close();
            } catch (Exception var13) {
                var13.printStackTrace();
            }

        }

        return sb.toString();
    }
}
