package com.swb.alibaba.common.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * <p>文件  Rest</p>
 * <p>时间  2021-06-30 23:17:39</p>
 *
 * @author swb
 */
@ConditionalOnMissingBean(
        name = {"restTemplate"}
)
@Configuration
public class RestTemplateConfig {
    @Autowired
    private HttpClientConfig httpClientConfig;

    public RestTemplateConfig( ) {
    }

    @Bean
    public RestTemplate restTemplate(ClientHttpRequestFactory factory) {
        return new RestTemplate(factory);
    }

    @Bean
    public ClientHttpRequestFactory simpleClientHttpRequestFactory() {
        SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        factory.setConnectTimeout(this.httpClientConfig.getConnectTimeout());
        factory.setReadTimeout(this.httpClientConfig.getReadTimeout());
        return factory;
    }
}
