package com.swb.alibaba.common.enums;

/**
 * <p>文件  IErrorEnum</p>
 * <p>时间  2021-06-30 22:37:49</p>
 *
 * @author swb
 */
public interface IErrorEnum {
    String getCode();

    Integer getCodeNumber();

    String getCodeNumString();

    String getMessage();
}