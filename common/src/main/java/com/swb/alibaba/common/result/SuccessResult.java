package com.swb.alibaba.common.result;

/**
 * <p>文件  SuccessResult</p>
 * <p>时间  2021-06-30 22:34:00</p>
 *
 * @author swb
 */
public class SuccessResult extends Result {
    private Object data;

    public SuccessResult() {
    }

    public SuccessResult(Object data) {
        this.data = data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Object getData() {
        return this.data;
    }
}
