package com.swb.alibaba.common.constant;

/**
 * <p>文件  RequestMethodConst</p>
 * <p>时间  2021-06-30 22:54:13</p>
 *
 * @author swb
 */
public class RequestMethodConst {
    public static final String POST = "POST";
    public static final String GET = "GET";
    public static final String PUT = "PUT";
    public static final String HEAD = "HEAD";
    public static final String DELETE = "DELETE";

    public RequestMethodConst() {
    }
}
