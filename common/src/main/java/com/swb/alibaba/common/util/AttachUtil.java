package com.swb.alibaba.common.util;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * <p>文件  AttachUtil</p>
 * <p>时间  2021-06-30 23:02:02</p>
 *
 * @author swb
 */
public class AttachUtil {
    private AttachUtil() {
    }

    public static void download(String fileName, byte[] fileContent, HttpServletResponse response) {
        response.setCharacterEncoding("utf-8");
        response.setContentType("multipart/form-data");
        response.setHeader("Content-Disposition", "attachment;fileName=" + new String(fileName.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1));

        try {
            BufferedOutputStream bufOut = new BufferedOutputStream(response.getOutputStream());
            bufOut.write(fileContent);
            bufOut.flush();
            bufOut.close();
        } catch (IOException var5) {
            var5.printStackTrace();
        }

    }

    public static void outPrintImage(byte[] fileContent, HttpServletResponse response) {
        if (fileContent != null && fileContent.length > 0) {
            try {
                ServletOutputStream out = response.getOutputStream();
                BufferedOutputStream bout = new BufferedOutputStream(out);
                bout.write(fileContent);
                bout.flush();
                bout.close();
                out.close();
            } catch (IOException var5) {
                var5.printStackTrace();
            }

        }
    }
}
