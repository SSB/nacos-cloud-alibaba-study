package com.swb.cloud.alibaba.consumer.controller;

import com.swb.alibaba.common.result.Result;
import com.swb.alibaba.common.result.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * <p>文件  DemoController</p>
 * <p>时间  2021-06-30 23:14:48</p>
 *
 * @author swb
 */
@RestController
public class DemoController {

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private DiscoveryClient discoveryClient;

    @GetMapping("/testNacos")
    public Result testNacos(){
        ServiceInstance serviceInstance = discoveryClient.getInstances("nacos-discovery-provider").get(0);
        return ResultUtil.success(restTemplate.getForObject(serviceInstance.getUri()+"/hello?name=consumer",String.class));
    }
}
