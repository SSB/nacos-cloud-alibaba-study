package com.swb.cloud.alibaba.consumer.controller;

import com.swb.alibaba.common.result.Result;
import com.swb.alibaba.common.result.ResultUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>文件  HelloController</p>
 * <p>时间  2021-06-30 21:55:01</p>
 *
 * @author swb
 */
@RestController
@Slf4j
public class HelloController {

    @GetMapping("/hello")
    public Result helloWho(String name) {
        log.info("name" + name);
        return ResultUtil.success("你好啊 " + name);
    }
}
